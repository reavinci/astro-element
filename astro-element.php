<?php
/**
 * Plugin Name: Astro Element
 * Description: Custom Widgets for Elementor page builder.
 * Plugin URI:  https://webforia.id/product/astro-element
 * Version:     1.7.0
 * Author:      Webforia
 * Author URI:  https://webforia.id
 * Text Domain: astro-element
 */
define('ASTRO_ELEMENT_TEMPLATE', plugin_dir_path(__FILE__));
define('ASTRO_ELEMENT_ASSETS', plugin_dir_url(__FILE__));

define('ASTRO_ELEMENT_NAME', 'Astro Element');
define('ASTRO_ELEMENT_SLUG','astro-element');
define('ASTRO_ELEMENT_DOMAIN', 'astro-element');
define('ASTRO_ELEMENT_VERSION', '1.7.0');
define('ASTRO_ELEMENT_URL', 'https: //webforia.id/product/astro-element');
define('ASTRO_ELEMENT_DOC', 'https: //www.panduan.webforia.id/astro-element');

/*=================================================;
/* LOAD THIS PLUGIN AFTER RETHEME LOAD
/*================================================= */
function ael_plugin_loaded()
{
    require_once ASTRO_ELEMENT_TEMPLATE . '/vendor/vendor.php';
    require_once ASTRO_ELEMENT_TEMPLATE . '/plugin.php';
    require_once ASTRO_ELEMENT_TEMPLATE . '/function.php';
    require_once ASTRO_ELEMENT_TEMPLATE . '/core/core.php';
    require_once ASTRO_ELEMENT_TEMPLATE . '/includes/include.php';
    require_once ASTRO_ELEMENT_TEMPLATE . '/widgets/widget.php';

    $check_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/astro-element', __FILE__, 'astro-element');
    $check_update->setBranch('stable_release');

}

ael_plugin_loaded();
