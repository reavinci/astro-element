<?php

/*=================================================
 * LIMIT THE CUNTENT
/*================================================= */
if (!function_exists('astro_the_content')) {
    function astro_the_content($excerpt_length = '')
    {   
    
        $excerpt = get_the_excerpt();

        $excerpt = substr($excerpt, 0, $excerpt_length);
        $result  = substr($excerpt, 0, strrpos($excerpt, ' '));

        return $result.'...';

    }
}

/*=================================================
 *  CONDITION TAGS
/*=================================================
 * @since 1.0.0
 * @desc cek plugin WooCommerce active
 * @param page WooCommerce page $page
 * @return boolean
 */

if (!function_exists('astro_is_woocommerce')) {
    function astro_is_woocommerce($page = '')
    {
        if (class_exists('WooCommerce')) {
            // Rertuns true on WooCommerce Plugins active
            if (empty($page)) {
                return true;
            }

            // all template WooCommerce but cart and checkout not include
            if ($page == 'pages' && is_woocommerce()) {
                return true;
            }

            // Returns true when on the product archive page (shop).
            if ($page == 'shop' && is_shop()) {
                return true;
            }

            if ($page == 'product' && is_product()) {
                return true;
            }

            // Returns true on the customer’s account pages.
            if ($page == 'account_page' && is_account_page()) {
                return true;
            }

            // Returns true on the cart page.
            if ($page == 'cart' && is_cart()) {
                return true;
            }

            // Returns true on the checkout page.
            if ($page == 'checkout' && is_checkout()) {
                return true;
            }

            // Returns true when viewing a WooCommerce endpoint
            if ($page == 'endpoint_url' && is_wc_endpoint_url()) {
                return true;
            }
        } else {
            return false;
        }
    }
}

/*=================================================;
/* CURRENCY
/*================================================= */
if (!function_exists('astro_currency')) {
    function astro_get_currency($value)
    {
        $currency = array(
            ''             => __('None', 'elementor-pro'),
            'dollar'       => '&#36;',
            'euro'         => '&#128;',
            'baht'         => '&#3647;',
            'franc'        => '&#8355;',
            'guilder'      => '&fnof;',
            'krona'        => 'kr',
            'lira'         => '&#8356;',
            'peseta'       => '&#8359',
            'peso'         => '&#8369;',
            'pound'        => '&#163;',
            'real'         => 'R$',
            'ruble'        => '&#8381;',
            'rupee'        => '&#8360;',
            'indian_rupee' => '&#8377;',
            'shekel'       => '&#8362;',
            'yen'          => '&#165;',
            'won'          => '&#8361;',
            'custom'       => __('Custom', 'elementor-pro'),
        );
        return $currency[$value];
    }
}

/**
 * Print image from elementor setting widgets
 */
if (!function_exists('astro_get_attachment_image')) {
    function astro_get_attachment_image($settings, $size = '')
    {

        if (!empty($settings['id'])) {
            $img = wp_get_attachment_image($settings['id'], $size);
        } else {
            $img = '<img src="' . $settings['url'] . '">';
        }

        return $img;

    }

}
/**
 * Print image url from elementor setting widgets
 */
if (!function_exists('astro_get_attachment_image_url')) {
    function astro_get_attachment_image_url($settings, $size = '')
    {
        $image_id  = !empty($setting['id']) ? $setting['id'] : '';
        $image_url = !empty($setting['url']) ? $setting['url'] : '';

        if (!empty($settings['id'])) {
            $img = wp_get_attachment_image_url($settings['id'], $size);
        } else {
            $img = $settings['url'];
        }

        return $img;
    }
}
