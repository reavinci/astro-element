<?php if(get_option('astro_element_gmap_api')): ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_option('astro_element_gmap_api')?>"></script>
<?php endif ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>

<div id="<?php echo 'astro-map-'.$this->get_id() ?>" class="ael-map js-astro-map" style="height: 470px"></div>

<script type="text/javascript">
var astroMap = function(){
  var map = new GMaps({
   el: '#<?php echo 'astro-map-'.$this->get_id() ?>',
   lat: <?php echo $settings['lat'] ?>,
   lng: <?php echo $settings['lng'] ?>,
   zoom: <?php echo $settings['map_zoom'] ?>,
   zoomControl : true,
   zoomControlOpt: {
       style : 'SMALL',
       // position: 'TOP_LEFT'
   },
   panControl : false,

   <?php if(!empty($snazzymaps)): ?>
    styles: <?php echo $snazzymaps ?>
   <?php endif; ?>
 });

  <?php foreach ( $settings['maker'] as $index => $maker ) :?>
  map.addMarker({
        lat: '<?php echo $maker['lat'] ?>',
        lng: '<?php echo $maker['lng'] ?>',
        <?php if(!empty($maker['content'])): ?>
        infoWindow: {
          content: '<?php echo $maker['name'] ?>'
        }
        <?php endif; ?>


      });
  <?php endforeach; ?>
}

</script>
