
<?php $style = $settings['social_style'] ?>

<div id="team-<?php echo $this->get_id()?>" class="ael-team">

    <div class="ael-team__thumbnail rt-img rt-img--full">
       <?php echo astro_get_attachment_image($settings['image'], $settings['image_size']) ?>
    </div>
    
    <div class="ael-team__body">
        <h4 class="ael-team__name"><?php echo $settings['name'] ?></h4>
        <span class="ael-team__position"><?php echo $settings['position'] ?></span>
        <div class="ael-team__content"><?php echo $settings['content'] ?></div>

        <?php if($settings['socmed']): ?>
        <div class="rt-socmed <?php echo 'rt-socmed--'.$style?>">
            <?php foreach ($settings['socmed'] as $key => $socmed): ?>

            <?php 
            $url_target = $socmed['link']['is_external'] ? ' target="_blank"' : '';
            $url_nofollow = $socmed['link']['nofollow'] ? ' rel="nofollow"' : '';
            ?>
            <a href="<?php $socmed['link']['url']?>" class="rt-socmed__item <?php echo $socmed['socmed_item']?>" <?php $url_target.' '.$url_nofollow?>>
                <i class="fa fa-<?php echo $socmed['socmed_item']?>"></i>
            </a>
            <?php endforeach?>
        </div>
        <?php endif ?>
    </div>

</div>