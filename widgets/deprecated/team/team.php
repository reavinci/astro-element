<?php
namespace Astro_Element\Elementor;

use Astro_Element\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Team extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-team';
    }

    public function get_title()
    {
        return __('Team', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-team';
    }

    public function get_categories()
    {
        return ['astro-element-deprecated'];
    }

    protected function setting_item()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );

        $this->add_control(
            'image',
            [
                'label' => __('Choose Image', 'astro-element'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'name',
            [
                'label' => __('Name', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('John Doe', 'astro-element'),
            ]
        );

        $this->add_control(
            'position',
            [
                'label' => __('Position', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('CEO', 'astro-element'),
            ]
        );

        $this->add_control(
            'content',
            [
                'label' => __('Content', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Team member personal information', 'astro-element'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'socmed_item', [
                'label' => __('Social Media', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'options' => Helper::get_social_media(),
            ]
        );

        $repeater->add_control(
            'link',
            [
                'label' => __('Link', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('#', 'astro-element'),
                'show_external' => true,
                'label_block' => false,
                'default' => [
                    'url' => '',
                    'is_external' => false,
                    'nofollow' => true,
                ],
            ]
        );

        $this->add_control(
            'socmed',
            [
                'label' => __('Social Media', 'astro-element'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'socmed_item' => __('facebook', 'astro-element'),
                    ],

                ],
                'title_field' => '{{{ socmed_item }}}',
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'social_style',
            [
                'label' => __('Color', 'elementor'),
                'type' => Controls_Manager::SELECT,
                'default' => 'brand',
                'options' => [
                    'brand' => __('Official Color', 'elementor'),
                    'border' => __('Border', 'elementor'),
                    'custom' => __('Custom', 'elementor'),
                ],
            ]
        );

        $this->end_controls_section();

    }

    public function style_name()
    {
        $this->start_controls_section(
            'style_name',
            [
                'label' => __('Name', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'name_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team__name' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'name_typography',
                'selector' => '{{WRAPPER}} .ael-team__name',
            ]
        );

        $this->add_responsive_control(
            'name_spacing',
            [
                'label' => __('Name Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-team__name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'general_background',
            [
                'label' => __('Background Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .ael-team',
            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-team' => 'border-radius: {{SIZE}}{{UNIT}}; overflow: hidden;',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_padding',
            [
                'label' => __('Padding', 'astro-element'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-team' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_position()
    {
        $this->start_controls_section(
            'style_position',
            [
                'label' => __('Position', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'position_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team__position' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'position_typography',
                'selector' => '{{WRAPPER}} .ael-team__position',
            ]
        );

        $this->add_responsive_control(
            'position_spacing',
            [
                'label' => __('Position Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-team__position' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .ael-team__content',
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label' => __('Position Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-team__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_social_media()
    {
        $this->start_controls_section(
            'style_social',
            [
                'label' => __('Social', 'elementor'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'border_radius',
            [
                'label' => __('Border Radius', 'elementor'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'social_normal',
            [
                'label' => __('Normal', 'rt_domain'),
            ]
        );

        $this->add_control(
            'icon_primary_color',
            [
                'label' => __('Primary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'icon_secondary_color',
            [
                'label' => __('Secondary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'social_border_color',
            [
                'label' => __('Border Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .ael-social__item' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'social_hover',
            [
                'label' => __('Hover', 'rt_domain'),
            ]
        );
        $this->add_control(
            'social_primary_color_hover',
            [
                'label' => __('Primary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '',

                'selectors' => [
                    '{{WRAPPER}} .ael-social__item:hover' => 'background-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'social_secondary_color_hover',
            [
                'label' => __('Secondary Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '',

                'selectors' => [
                    '{{WRAPPER}} .ael-social__item:hover' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'social_border_color_hover',
            [
                'label' => __('Border Color', 'elementor'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .ael-social__item:hover' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end social media */

        $this->end_controls_section();
    }

    protected function _register_controls()
    {
        $this->setting_item();

        $this->style_general();
        $this->style_name();
        $this->style_position();
        $this->style_content();
        $this->style_social_media();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        include dirname(__FILE__) . '/team-view.php';

    }
}
