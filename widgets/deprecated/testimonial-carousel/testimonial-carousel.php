<?php
namespace Astro_Element\Elementor;

use Astro_Element\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Testimonial_Carousel extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-testimonial-carousel';
    }

    public function get_title()
    {
        return __('Testimonial Carousel', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-testimonial';
    }

    public function get_categories()
    {
        return ['astro-element-deprecated'];
    }

    public function setting_item()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );
        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'content', [
                'label' => __('Content', 'astro-element'),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'image',
            [
                'label' => __('Choose Image', 'astro-element'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );
        $repeater->add_control(
            'name', [
                'label' => __('Name', 'astro-element'),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $repeater->add_control(
            'position', [
                'label' => __('Position', 'astro-element'),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'testimonial',
            [
                'label' => __('Testimonial', 'astro-element'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('I am slide content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'astro-element'),
                    ],
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('I am slide content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'astro-element'),
                    ],
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('I am slide content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'astro-element'),
                    ],

                ],
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->end_controls_section();

    }


    public function style_name()
    {
        $this->start_controls_section(
            'style_name',
            [
                'label' => __('Name', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'name_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__name' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'name_typography',
                'selector' => '{{WRAPPER}} .ael-testimonial__name',
            ]
        );

        $this->add_responsive_control(
            'name_spacing',
            [
                'label' => __('Name Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_position()
    {
        $this->start_controls_section(
            'style_position',
            [
                'label' => __('Position', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'position_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team__position' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'position_typography',
                'selector' => '{{WRAPPER}} .ael-team__position',
            ]
        );

        $this->add_responsive_control(
            'position_spacing',
            [
                'label' => __('Position Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-team__position' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'content_background',
            [
                'label' => __('Background Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__content' => 'background-color: {{VALUE}};border-color: {{VALUE}};',
                    '{{WRAPPER}} .ael-testimonial__content:before' => 'background-color: {{VALUE}};border-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .ael-testimonial__content',
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label' => __('Position Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

         $this->add_responsive_control(
            'content_alignment',
            [
                'label' => __('Alignment', 'astro-element'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-element'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-element'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__content' => 'text-align: {{VALUE}};',
                ],
            ]
        );
    

        $this->end_controls_section();
    }

    
    protected function _register_controls()
    {
        $this->setting_item();

        $this->style_name();
        $this->style_position();
        $this->style_content();

        $this->setting_carousel([
            'carousel' => 'false',
        ]);
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        echo HTML::before_slider(array(
            'id' => 'ael-slider-' . $this->get_id(),
            'items-lg' => $settings['slider_item'],
            'items-md' => $settings['slider_item_tablet'],
            'items-sm' => $settings['slider_item_mobile'],
            'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
            'gap' => $settings['slider_gap'],
            'pagination' => ($settings['slider_pagination'] === 'yes') ? true : false,
            'loop' => $settings['slider_loop'],
            'autoplay' => $settings['slider_auto_play'],
        ));

        echo HTML::open('ael-slider__main owl-carousel');

        foreach ($settings['testimonial'] as $key => $testimonial) {
            include dirname(__FILE__) . '/testimonial-carousel-view.php';
        }
        echo HTML::close();

        echo HTML::after_slider();

    }
}
