<?php
namespace Astro_Element\Elementor;

use Astro_Element\Helper;
use Astro_Element\HTML;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Team_Carousel extends Team
{
    public function get_name()
    {
        return 'astro-team-carousel';
    }

    public function get_title()
    {
        return __('Team Carousel', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-team';
    }

    public function get_categories()
    {
        return ['astro-element-deprecated'];
    }

    protected function setting_item()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'image',
            [
                'label' => __('Choose Image', 'astro-element'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'name',
            [
                'label' => __('Name', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('John Doe', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'position',
            [
                'label' => __('Position', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('CEO', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'content',
            [
                'label' => __('Content', 'astro-element'),
                'type' => Controls_Manager::TEXTAREA,
                'default' => __('Team member personal information', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'link_facebook',
            [
                'label' => __('Facebook', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $repeater->add_control(
            'link_instagram',
            [
                'label' => __('Instagram', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $repeater->add_control(
            'link_youtube',
            [
                'label' => __('Youtube', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $repeater->add_control(
            'link_linkedin',
            [
                'label' => __('Linkedin', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

         $repeater->add_control(
            'link_email',
            [
                'label' => __('Email', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );



        $this->add_control(
            'team',
            [
                'label' => __('Team', 'astro-element'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('Team member personal information'),
                    ],
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('Team member personal information'),
                    ],
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('Team member personal information', 'astro-element'),
                    ],

                ],
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->end_controls_section();

    }

    public function setting_options()
    {
        $this->start_controls_section(
            'setting_optjon',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );
        $this->add_control(
            'is_external',
            [
                'label' => __('External Window', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'yes',
                'options' => [
                    'yes' => 'Yes',
                    'no' => 'No',
                ],
            ]
        );

        $this->add_control(
            'nofollow',
            [
                'label' => __('Nofollow', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no',
                'options' => [
                    'yes' => 'Yes',
                    'no' => 'No',
                ],
            ]
        );

        $this->add_control(
            'social_style',
            [
                'label' => __('Color', 'elementor'),
                'type' => Controls_Manager::SELECT,
                'default' => 'brand',
                'options' => [
                    'brand' => __('Official Color', 'elementor'),
                    'border' => __('Border', 'elementor'),
                    'custom' => __('Custom', 'elementor'),
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function _register_controls()
    {
        $this->setting_item();
        $this->setting_options();

        $this->style_general();
        $this->style_name();
        $this->style_position();
        $this->style_content();
        $this->style_social_media();

        $this->setting_carousel([
            'carousel' => 'false',
        ]);
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        echo HTML::before_slider(array(
            'id' => 'ael-slider-' . $this->get_id(),
            'items-lg' => $settings['slider_item'],
            'items-md' => $settings['slider_item_tablet'],
            'items-sm' => $settings['slider_item_mobile'],
            'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
            'gap' => $settings['slider_gap'],
            'pagination' => ($settings['slider_pagination'] === 'yes') ? true : false,
            'loop' => $settings['slider_loop'],
            'autoplay' => $settings['slider_auto_play'],
        ));

        echo HTML::open('ael-slider__main owl-carousel');

        foreach ($settings['team'] as $key => $team) {
            include dirname(__FILE__) . '/team-carousel-view.php';
        }
        echo HTML::close();

        echo HTML::after_slider();
        

    }
}
