<?php
namespace Astro_Element\Elementor;

use Astro_Element\Elementor_Base;
use Astro_Element\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Team_Box extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-team-box';
    }

    public function get_title()
    {
        return __('Team', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-team';
    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function setting_item()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'image',
            [
                'label'   => __('Choose Image', 'astro-element'),
                'type'    => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'name',
            [
                'label'   => __('Name', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => __('John Doe', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'position',
            [
                'label'   => __('Position', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => __('CEO', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'content',
            [
                'label'   => __('Content', 'astro-element'),
                'type'    => Controls_Manager::TEXTAREA,
                'default' => __('Team member personal information', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'link_facebook',
            [
                'label'         => __('Facebook', 'astro-element'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $repeater->add_control(
            'link_instagram',
            [
                'label'         => __('Instagram', 'astro-element'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $repeater->add_control(
            'link_youtube',
            [
                'label'         => __('Youtube', 'astro-element'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $repeater->add_control(
            'link_linkedin',
            [
                'label'         => __('Linkedin', 'astro-element'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $repeater->add_control(
            'link_email',
            [
                'label'         => __('Email', 'astro-element'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => __('#', 'astro-element'),
                'show_external' => false,
            ]
        );

        $this->add_control(
            'teams',
            [
                'label'       => __('Team', 'astro-element'),
                'type'        => Controls_Manager::REPEATER,
                'fields'      => $repeater->get_controls(),
                'default'     => [
                    [
                        'name'     => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content'  => __('Team member personal information'),
                    ],
                    [
                        'name'     => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content'  => __('Team member personal information'),
                    ],
                    [
                        'name'     => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content'  => __('Team member personal information', 'astro-element'),
                    ],

                ],
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->end_controls_section();

    }

    public function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label'           => __('Column', 'astro-element'),
                'type'            => Controls_Manager::SELECT,
                'options'         => [
                    1 => __(1, 'astro-element'),
                    2 => __(2, 'astro-element'),
                    3 => __(3, 'astro-element'),
                    4 => __(4, 'astro-element'),
                    6 => __(6, 'astro-element'),
                ],
                'devices'         => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default'  => 2,
                'mobile_default'  => 1,
                'condition'       => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label'        => __('Masonry', 'astro-element'),
                'type'         => Controls_Manager::SWITCHER,
                'default'      => 'no',
                'label_off'    => __('Off', 'astro-element'),
                'label_on'     => __('On', 'astro-element'),
                'return_value' => 'yes',
                'condition'    => [
                    'carousel!' => 'yes',
                ],
            ]
        );
        $this->add_control(
            'image_size',
            [
                'label'   => __('Image Size', 'astro-element'),
                'type'    => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );
        $this->add_control(
            'is_external',
            [
                'label'   => __('External Window', 'astro-element'),
                'type'    => Controls_Manager::SELECT,
                'default' => 'yes',
                'options' => [
                    'yes' => 'Yes',
                    'no'  => 'No',
                ],
            ]
        );

        $this->add_control(
            'nofollow',
            [
                'label'   => __('Nofollow', 'astro-element'),
                'type'    => Controls_Manager::SELECT,
                'default' => 'no',
                'options' => [
                    'yes' => 'Yes',
                    'no'  => 'No',
                ],
            ]
        );

        $this->add_control(
            'social_style',
            [
                'label'   => __('Color', 'elementor'),
                'type'    => Controls_Manager::SELECT,
                'default' => 'brand',
                'options' => [
                    'brand'  => __('Official Color', 'elementor'),
                    'border' => __('Border', 'elementor'),
                    'custom' => __('Custom', 'elementor'),
                ],
            ]
        );

        $this->end_controls_section();

    }
    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'general_background',
            [
                'label'     => __('Background Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'     => 'general_border',
                'selector' => '{{WRAPPER}} .ael-team',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'     => 'general_shadow',
                'selector' => '{{WRAPPER}} .ael-team',
            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label'      => __('Border Radius', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-team' => 'border-radius: {{SIZE}}{{UNIT}}; overflow: hidden;',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_name()
    {
        $this->start_controls_section(
            'style_name',
            [
                'label' => __('Name', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'name_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team__name' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'name_typography',
                'selector' => '{{WRAPPER}} .ael-team__name',
            ]
        );

        $this->add_responsive_control(
            'name_spacing',
            [
                'label'      => __('Name Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                    '%'  => [
                        'min'  => 10,
                        'max'  => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-team__name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_position()
    {
        $this->start_controls_section(
            'style_position',
            [
                'label' => __('Position', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'position_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team__position' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'position_typography',
                'selector' => '{{WRAPPER}} .ael-team__position',
            ]
        );

        $this->add_responsive_control(
            'position_spacing',
            [
                'label'      => __('Position Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                    '%'  => [
                        'min'  => 10,
                        'max'  => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-team__position' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-team__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'content_typography',
                'selector' => '{{WRAPPER}} .ael-team__content',
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label'      => __('Position Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                    '%'  => [
                        'min'  => 10,
                        'max'  => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-team__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_social_media()
    {
        $this->start_controls_section(
            'style_social',
            [
                'label' => __('Social', 'elementor'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'border_radius',
            [
                'label'      => __('Border Radius', 'elementor'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .rt-socmed__item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'social_normal',
            [
                'label' => __('Normal', 'rt_domain'),
            ]
        );

        $this->add_control(
            'icon_primary_color',
            [
                'label'     => __('Primary Color', 'elementor'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'icon_secondary_color',
            [
                'label'     => __('Secondary Color', 'elementor'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-socmed__item' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'social_border_color',
            [
                'label'     => __('Border Color', 'elementor'),
                'type'      => Controls_Manager::COLOR,
                'default'   => '',
                'selectors' => [
                    '{{WRAPPER}} .ael-social__item' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'social_hover',
            [
                'label' => __('Hover', 'rt_domain'),
            ]
        );
        $this->add_control(
            'social_primary_color_hover',
            [
                'label'     => __('Primary Color', 'elementor'),
                'type'      => Controls_Manager::COLOR,
                'default'   => '',

                'selectors' => [
                    '{{WRAPPER}} .ael-social__item:hover' => 'background-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'social_secondary_color_hover',
            [
                'label'     => __('Secondary Color', 'elementor'),
                'type'      => Controls_Manager::COLOR,
                'default'   => '',

                'selectors' => [
                    '{{WRAPPER}} .ael-social__item:hover' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'social_border_color_hover',
            [
                'label'     => __('Border Color', 'elementor'),
                'type'      => Controls_Manager::COLOR,
                'default'   => '',
                'selectors' => [
                    '{{WRAPPER}} .ael-social__item:hover' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end social media */

        $this->end_controls_section();
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_item();
        $this->setting_options();

        $this->style_general();
        $this->style_name();
        $this->style_position();
        $this->style_content();
        $this->style_social_media();

        $this->setting_carousel();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->element_before_loop($settings, $this->get_id());

        foreach ($settings['teams'] as $key => $team) {
            include dirname(__FILE__) . '/team-box-view.php';
        }

        $this->element_after_loop($settings);

    }
}
