<div class="ael-advanced-heading">
	<?php if ($settings['subtitle']): ?>
		<span class="ael-advanced-heading__subtitle"><?php echo $settings['subtitle'] ?></span>
	<?php endif ?>
    

    <?php if ($settings['title']): ?>
    	<h2 class="ael-advanced-heading__title"><?php echo $settings['title'] ?></h2>
    <?php endif ?>
    

    <?php if ($settings['desc']): ?>
     <p class="ael-advanced-heading__desc"><?php echo $settings['desc'] ?></p>
    <?php endif ?>

    <?php if ($settings['shadow']): ?>
     <div class="ael-advanced-heading__before-shadow">
         <span class="ael-advanced-heading__shadow"><?php echo $settings['shadow'] ?></span>
     </div>
    <?php endif ?>
    
</div>