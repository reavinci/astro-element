<?php 
 $date = str_replace('-', '/',$settings['date']);
?>

<div id="<?php echo 'countdown-'.$this->get_id()?>" class="ael-countdown js-countdown" data-date="<?php echo $date?>">

    <?php if(!empty($settings['day_label'])): ?>
    <div class="ael-countdown__item">
        <div class="ael-countdown__number js-countdown-day">00</div>
        <div class="ael-countdown__title"><?php echo $settings['day_label'] ?></div>
    </div>
    <?php endif ?>

    <?php if(!empty($settings['hour_label'])): ?>
    <div class="ael-countdown__item">
        <div class="ael-countdown__number js-countdown-hour">00</div>
        <div class="ael-countdown__title"><?php echo $settings['hour_label'] ?></div>
    </div>
    <?php endif ?>

    <?php if(!empty($settings['minute_label'])): ?>
    <div class="ael-countdown__item">
        <div class="ael-countdown__number js-countdown-minute">00</div>
        <div class="ael-countdown__title"><?php echo $settings['minute_label'] ?></div>
    </div>
    <?php endif ?>

    <?php if(!empty($settings['second_label'])): ?>
    <div class="ael-countdown__item">
        <div class="ael-countdown__number js-countdown-second">00</div>
        <div class="ael-countdown__title"><?php echo $settings['second_label'] ?></div>
    </div>
    <?php endif ?>
 
</div>