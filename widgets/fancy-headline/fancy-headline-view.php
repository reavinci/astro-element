<?php $headline = explode(",",$settings['headline']); ?>

<div class="ael-headline js-astro-headline" id="headline-<?php echo $this->get_id()?>" data-loop="<?php echo $settings['loop']?>" data-speed="<?php echo $settings['speed']?>">

	<div class="ael-headline__data">
		<?php foreach ($headline as $key => $item): ?>
			<p><?php echo $item?></p>
		<?php endforeach;?>
	</div>

	<div class="ael-headline__wrapper">

		<?php if (!empty($settings['before_headline'])): ?>
			<span class="ael-headline__regular"><?php echo $settings['before_headline']?></span>
		<?php endif;?>

		<span class="ael-headline__value"></span>

		<?php if (!empty($settings['after_headline'])): ?>
			<span class="ael-headline__regular"><?php echo $settings['after_headline']?></span>
		<?php endif;?>


	</div>

</div>
