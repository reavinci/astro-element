<?php

namespace Astro_Element\Elementor;

use Astro_Element\Elementor_Base;
use Astro_Element\Helper;
use Elementor;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Product extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-product';
    }

    public function get_title()
    {
        return __('Products', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-product';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_option();

        $this->style_general();

        $this->setting_carousel();

    }

    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'astro-element'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'astro-element'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'product',
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Number', 'astro-element'),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'astro-element'),
                'type' => Controls_Manager::HEADING,
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'astro-element'),
                    'featured' => __('Featured Products', 'astro-element'),
                    'category' => __('Category', 'astro-element'),
                    'manually' => __('Manually Id', 'astro-element'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Category', 'astro-element'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'default' => 'lastest',
                'options' => Helper::get_terms('product_cat'),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Product', 'astro-element'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts('product'),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'astro-element'),
                    'total_sales' => __('Most Selling', 'astro-element'),
                    'wp_post_views_count' => __('Most Viewer', 'astro-element'),
                    'comment_count' => __('Most Review', 'astro-element'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'astro-element'),
                    'DESC' => __('DESC', 'astro-element'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'astro-element'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'condition' => [
                    'posts_post_type!' => 'by_id',
                ],
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'astro-element'),
            ]
        );
        $this->end_controls_section();
    }

    protected function setting_option()
    {
        $this->start_controls_section(
            'setting_product',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro-element'),
                'label_on' => __('On', 'astro-element'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro-element'),
                    2 => __(2, 'astro-element'),
                    3 => __(3, 'astro-element'),
                    4 => __(4, 'astro-element'),
                    6 => __(6, 'astro-element'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );

        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'widgets/product/product-view',
            'class_wrapper' => 'products woocommerce flex-woocommerce',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
}
