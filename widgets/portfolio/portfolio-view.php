<?php $post_classes = !empty($settings['class'])?$settings['class']:""; ?>

<div <?php post_class($post_classes) ?>>
    <div class="ael-portfolio__thumbnail rt-img rt-img--full">
        <img src="http://demo.phlox.pro/portfolio-minimal/wp-content/uploads/sites/25/2018/04/a5885594-0c8e-41e6-8c18-3783e2930b6e-240x240.jpg" alt="">
        <div class="ael-portfolio__overlay">
            <div class="ael-portfolio__body">
                <div class="ael-portfolio__action">
                    <a href="#" ><?php _e('Zoom', 'astro-element') ?></a>
                    <a href="#" ><?php _e('View', 'astro-element') ?></a>
                </div>

                <?php if($settings['portfolio_type'] == 'overlay'): ?>
                    <h3 class="ael-portfolio__title"><a href="#">Resume Amelia</a></h3>

                    <?php if($settings['meta_category']): ?>
                    <div class="ael-portfolio__category">
                        <a>Design</a>
                        <a>Illustration</a>
                    </div>
                    <?php endif ?>

                <?php endif ?>

            </div>
        </div>
    </div>

    <?php if($settings['portfolio_type'] != 'overlay'): ?>
    <div class="ael-portfolio__body">
        <h3 class="ael-portfolio__title">Resume Amelia</h3>

        <?php if ($settings['meta_category']): ?>
        <div class="ael-portfolio__category">
            <a>Design</a>
            <a>Illustration</a>
        </div>
        <?php endif?>

    </div>
    <?php endif ?>

</div>