<?php
namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Astro_Element\Elementor_Base;
use Astro_Element\Helper;
use Astro_Element\HTML;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Image_Slider extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-image-slider';
    }

    public function get_title()
    {
        return __('Image Slider', 'astro-element');
    }

    public function get_icon()
    {
         return 'ate-icon ate-post-slider';

    }

    public function get_categories()
    {
        return ['astro-element'];

    }

    protected function _register_controls()
    {
        $this->setting_options(); //protected
        $this->setting_content();

        // extend retheme base
        $this->setting_carousel(array(
            'carousel' => true,
        ));
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'large',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->end_controls_section();
    }

    public function setting_content()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'image',
            [
                'label' => __('Choose Image Desktop', 'astro-element'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'image_mobile',
            [
                'label' => __('Choose Image Mobile', 'astro-element'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'title', [
                'label' => __('Title', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Title', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'link',
            [
                'label' => __('url', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'astro-element'),
                'show_external' => true,
                'label_block' => false,
                'default' => [
                    'url' => '',
                    'is_external' => false,
                    'nofollow' => true,
                ],
            ]
        );

        $this->add_control(
            'sliders',
            [
                'label' => __('iTEMS', 'astro-element'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'title' => __('Slide #1', 'astro-element'),
                    ],
                ],
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $classes[] = 'ael-slider js-astro-slider';

        echo HTML::before_slider(array(
            'id' => 'ael-slider-' . $this->get_id(),
            'class' => $classes,
            'items-lg' => $settings['slider_item'],
            'items-md' => $settings['slider_item_tablet'],
            'items-sm' => $settings['slider_item_mobile'],
            'pagination' => $settings['slider_pagination'],
            'gap' => $settings['slider_gap'],
            'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
            'autoplay' => $settings['slider_auto_play'],
        ));

        echo HTML::open('ael-slider__main owl-carousel');

        foreach ($settings['sliders'] as $key => $slider) {
            include dirname(__FILE__) . '/image-slider-view.php';
        }

        echo HTML::close();

        echo HTML::after_slider();

    }
}
