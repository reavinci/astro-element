<?php

namespace Astro_Element\Elementor;

use Astro_Element\Elementor_Base;
use Astro_Element\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Product_Cat extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-product-cat';
    }

    public function get_title()
    {
        return __('Products Category', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-product';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_option();

        $this->style_general();
        $this->style_image();
        $this->style_title();
        $this->style_content();

        $this->setting_carousel();

    }

    public function setting_option()
    {
        $this->start_controls_section(
            'term_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );
        $this->add_control(
            'term_child',
            [
                'label' => __('Child Category', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-element'),
                'label_off' => __('Off', 'astro-element'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'term_empty',
            [
                'label' => __('Hide Empty', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-element'),
                'label_off' => __('Off', 'astro-element'),
                'return_value' => 'yes',
            ]
        );
        $this->add_control(
            'term_title',
            [
                'label' => __('Title', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-element'),
                'label_off' => __('Off', 'astro-element'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'term_desc',
            [
                'label' => __('Description', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-element'),
                'label_off' => __('Off', 'astro-element'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'term_count',
            [
                'label' => __('Count', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-element'),
                'label_off' => __('Off', 'astro-element'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'term_count_text',
            [
                'label' => __('Count Text', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Items', 'astro-element'),
                'condition' => [
                    'term_count' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro-element'),
                'label_on' => __('On', 'astro-element'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro-element'),
                    2 => __(2, 'astro-element'),
                    3 => __(3, 'astro-element'),
                    4 => __(4, 'astro-element'),
                    6 => __(6, 'astro-element'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'thumbnail',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_alignment',
            [
                'label' => __('Layout Alignment', 'astro-element'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-element'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-element'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box' => 'text-align: {{VALUE}};',
                    '{{WRAPPER}} .ael-term-box__thumbnail' => 'margin-left: auto; margin-right: auto;',
                ],
            ]
        );

        // start tabs
        $this->start_controls_tabs('term_tabs');

        $this->start_controls_tab(
            'term_normal',
            [
                'label' => __('Normal', 'astro-element'),
            ]
        );
        $this->add_control(
            'general_background',
            [
                'label' => __('Background Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .ael-term-box',
            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            'term_hover',
            [
                'label' => __('Hover', 'astro-element'),
            ]
        );
        $this->add_control(
            'general_background_hover',
            [
                'label' => __('Background Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box:hover' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border_hover:hover',
                'selector' => '{{WRAPPER}} .ael-term-box',
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        // end tabs

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box' => 'border-radius: {{SIZE}}{{UNIT}}; overflow: hidden;',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_padding',
            [
                'label' => __('Padding', 'astro-element'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_image()
    {
        $this->start_controls_section(
            'style_image',
            [
                'label' => __('Image', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'image_radius',
            [
                'label' => __('Image Radius', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box__thumbnail' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'image_width',
            [
                'label' => __('Image Width', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 300,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box__thumbnail' => 'width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

         $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'image_border',
                'selector' => '{{WRAPPER}} .ael-term-box__thumbnail',
            ]
        );

        $this->add_responsive_control(
            'image_spacing',
            [
                'label' => __('Image Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box__thumbnail' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .ael-term-box__title',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box__title a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'title_spacing',
            [
                'label' => __('Title Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .ael-term-box__content',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label' => __('Title Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-term-box__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->element_before_loop($settings, $this->get_id());

        $terms = get_terms(array(
            'taxonomy' => 'product_cat',
            'hide_empty' => ($settings['term_empty'] == 'yes') ? true : false,
            'parent' => ($settings['term_child'] == 'yes') ? '' : 0,
        ));

        foreach ($terms as $key => $term) {
            if ($term->name !== 'uncategorized') {
                $thumbnail_id = get_term_meta($term->term_id, 'thumbnail_id', true);
                include dirname(__FILE__) . '/product-cat-view.php';
            }
        }

        $this->element_after_loop($settings);

    }
}
