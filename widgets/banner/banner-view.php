<?php 
$btn_primary_target = $data->button_primary_link['is_external'] ? ' target="_blank"' : '';
$btn_primary_nofollow = $data->button_primary_link['nofollow'] ? ' rel="nofollow"' : '';
$btn_second_target = $data->button_second_link['is_external'] ? ' target="_blank"' : '';
$btn_second_nofollow = $data->button_second_link['nofollow'] ? ' rel="nofollow"' : '';
?>
<div class="ael-banner">
    <div class="ael-banner__inner">
         <div class="ael-banner__body">

            <?php if(!empty($data->title)): ?>
            <h4 class="ael-banner__title"><?php echo $data->title ?></h4>
            <?php endif ?>

             <?php if(!empty($data->subtitle)): ?>
            <h5 class="ael-banner__subtitle"><?php echo $data->subtitle ?></h5>
            <?php endif ?>

             <?php if(!empty($data->subtitle)): ?>
            <p class="ael-banner__content"><?php echo $data->content ?></p>
            <?php endif ?>

            <div class="ael-banner__button">

                <?php if(!empty($data->button_primary)): ?>
                    <a class="rt-btn rt-btn--primary" href="<?php echo $data->button_primary_link['url']?>" <?php echo  $btn_primary_target . $btn_primary_nofollow?>><?php echo $data->button_primary?></a>
                <?php endif ?>

                <?php if(!empty($data->button_second)): ?>
                    <a class="rt-btn rt-btn--second" href="<?php echo $data->button_second_link['url']?>" <?php echo  $btn_second_target . $btn_second_nofollow?>><?php echo $data->button_second?></a>
                <?php endif ?>
                
            </div>
        </div>
    </div>
   
</div>