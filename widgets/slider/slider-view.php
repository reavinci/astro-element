<?php 
$btn_primary_target = $slider['button_primary_link']['is_external'] ? ' target="_blank"' : '';
$btn_primary_nofollow = $slider['button_primary_link']['nofollow'] ? ' rel="nofollow"' : '';
$btn_second_target = $slider['button_second_link']['is_external'] ? ' target="_blank"' : '';
$btn_second_nofollow = $slider['button_second_link']['nofollow'] ? ' rel="nofollow"' : '';
?>
<div  class="ael-slider__item">

   <div class="ael-slider__thumbnail" style="background-image: url('<?php echo astro_get_attachment_image_url($slider['image'], $settings['image_size']) ?>');"></div>
    <div class="ael-slider__bg"></div>
   <div class="ael-slider__inner">
     <div class="ael-slider__body">

        <?php if(!empty($slider['title'])): ?>
        <h3 class="ael-slider__title"><?php echo $slider['title'] ?></h3>
        <?php endif ?>

        <?php if(!empty($slider['subtitle'])): ?>
        <h4 class="ael-slider__subtitle"><?php echo $slider['subtitle'] ?></h4>
        <?php endif ?>

        <?php if(!empty($slider['content'])): ?>
        <p class="ael-slider__content"><?php echo $slider['content'] ?></p>
        <?php endif ?>

        <div class="ael-slider__button">
            <?php if(!empty($slider['button_primary'])): ?>
           <a class="rt-btn rt-btn--primary" href="<?php echo $slider['button_primary_link']['url']?>" <?php echo  $btn_primary_target . $btn_primary_nofollow?>><?php echo $slider['button_primary']?></a>
         <?php endif ?>
          <?php if(!empty($slider['button_second'])): ?>
           <a class="rt-btn rt-btn--second" href="<?php echo $slider['button_second_link']['url']?>" <?php echo  $btn_second_target . $btn_second_nofollow?>><?php echo $slider['button_second']?></a>
         <?php endif ?>
        </div>

    </div>
   </div>
   
</div>

