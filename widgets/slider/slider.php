<?php
namespace Astro_Element\Elementor;

use Astro_Element\Helper;
use Astro_Element\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Slider extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-slider';
    }

    public function get_title()
    {
        return __('Slider', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-slider';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {
        $this->setting_options(); //protected
        $this->setting_content();

        $this->style_general();
        $this->style_caption();
        $this->style_title();
        $this->style_subtitle();
        $this->style_content();

        $this->setting_button(array(
            'name' => 'button-1',
            'label' => 'Button 1',
            'class' => '.rt-btn--primary',
        ));
        $this->setting_button(array(
            'name' => 'button-2',
            'label' => 'Button 2',
            'class' => '.rt-btn--second',
        ));

        // extend astro base
        $this->setting_carousel(array(
            'carousel' => true,
        ));

    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'large',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_responsive_control(
            'slider_height',
            [
                'label' => __('Height', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 370,
                ],
                'range' => [
                    'px' => [
                        'min' => 100,
                        'max' => 700,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],

                'selectors' => [
                    '{{WRAPPER}} .ael-slider__thumbnail' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function setting_content()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'image',
            [
                'label' => __('Choose Image', 'astro-element'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'title', [
                'label' => __('Title', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Title', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'subtitle', [
                'label' => __('Sub Title', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Sub Title', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'content', [
                'label' => __('Content', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'button_primary', [
                'label' => __('Primary Button Text', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Button 1', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'button_primary_link',
            [
                'label' => __('Button Primary Link', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'astro-element'),
                'show_external' => true,
                'label_block' => false,
                'default' => [
                    'url' => '',
                    'is_external' => false,
                    'nofollow' => true,
                ],
            ]
        );

        $repeater->add_control(
            'button_second', [
                'label' => __('Secondary Button Text', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Button 2', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'button_second_link',
            [
                'label' => __('Button Second Link', 'astro-element'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'astro-element'),
                'show_external' => true,
                'label_block' => false,
                'default' => [
                    'url' => '',
                    'is_external' => false,
                    'nofollow' => true,
                ],
            ]
        );

        $this->add_control(
            'sliders',
            [
                'label' => __('Items', 'astro-element'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'title' => __('Slide #1', 'astro-element'),
                        'subtitle' => __('Sub Title #1', 'astro-element'),
                        'content' => __('Item content. Click the edit button to change this text.', 'astro-element'),
                    ],
                    [
                        'title' => __('Slide #2', 'astro-element'),
                        'subtitle' => __('Sub Title #2', 'astro-element'),
                        'content' => __('Item content. Click the edit button to change this text.', 'astro-element'),
                    ],
                ],
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();

    }
    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'general_background',
                'selector' => '{{WRAPPER}} .ael-slider .ael-slider__bg',
            ]
        );

        $this->end_controls_section();
    }

    public function style_caption()
    {
        $this->start_controls_section(
            'style_caption',
            [
                'label' => __('Caption', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'caption_background',
                'selector' => '{{WRAPPER}} .ael-slider__body',
            ]
        );
        $this->add_responsive_control(
            'caption_width',
            [
                'label' => __('Width', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__body' => 'max-width: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'caption_alignment',
            [
                'label' => __('Layout Alignment', 'astro-element'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-element'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-element'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__inner' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'caption_horizontal_position',
            [
                'label' => __('Horizontal Position', 'astro-element'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'default' => 'left',
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-element'),
                        'icon' => 'eicon-h-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon' => 'eicon-h-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-element'),
                        'icon' => 'eicon-h-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__inner' => 'justify-content: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'caption_vertical_position',
            [
                'label' => __('Vertical Position', 'astro-element'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'default' => 'end',
                'options' => [
                    'flex-start' => [
                        'title' => __('Top', 'astro-element'),
                        'icon' => 'eicon-v-align-top',
                    ],
                    'center' => [
                        'title' => __('Middle', 'astro-element'),
                        'icon' => 'eicon-v-align-middle',
                    ],
                    'flex-end' => [
                        'title' => __('Bottom', 'astro-element'),
                        'icon' => 'eicon-v-align-bottom',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__inner' => 'align-items: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'caption_padding',
            [
                'label' => __('Padding', 'astro-element'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%', 'em'],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }
    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .ael-slider__title',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'title_spacing',
            [
                'label' => __('Title Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_subtitle()
    {
        $this->start_controls_section(
            'style_subtitle',
            [
                'label' => __('Sub Title', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'subtitle_typography',
                'selector' => '{{WRAPPER}} .ael-slider__subtitle',
            ]
        );

        $this->add_control(
            'subtitle_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__subtitle' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'subtitle_spacing',
            [
                'label' => __('Sub Title Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__subtitle' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .ael-slider__content',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label' => __('Content Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-slider__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $classes[] = !empty($settings['style']) ? $settings['style'] : "";
        $classes[] = 'ael-slider js-astro-slider';

        echo HTML::before_slider(array(
            'id' => 'astro-slider-' . $this->get_id(),
            'class' => $classes,
            'items-lg' => !empty($settings['slider_item']['size']) ? $settings['slider_item']['size'] : $settings['slider_item'],
            'items-md' => !empty($settings['slider_item_tablet']['size']) ? $settings['slider_item_tablet']['size'] : $settings['slider_item_tablet'],
            'items-sm' => !empty($settings['slider_item_mobile']['size']) ? $settings['slider_item_mobile']['size'] : $settings['slider_item_mobile'],
            'pagination' => $settings['slider_pagination'],
            'gap' => $settings['slider_gap'],
            'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
        ));

        echo HTML::open('ael-slider__main owl-carousel');

        foreach ($settings['sliders'] as $key => $slider) {
            include dirname(__FILE__) . '/slider-view.php';
        }

        echo HTML::close();

        echo HTML::after_slider();

    }
}
