<?php

function ael_is_dev()
{
    if (get_option('astro_element_dev') == 'active') {
        return true;
    }
}

function ael_get_template_part($template, $data = '')
{
    $loader = new AEL_Template_Loader;

    $loader->set_template_data($data)
        ->get_template_part($template);

}