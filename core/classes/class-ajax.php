<?php
/**
 * This class handle ajax loop post or CPT
 *
 * @author webforia studio
 * @since 1.0.0
 */
namespace Astro_Element;

use Astro_Element\Helper;
use Astro_Element\HTML;


class Class_Ajax
{
    public function __construct()
    {
        add_action('wp_ajax_astro_ajax_loop_result', [$this, 'loop_result']);
        add_action('wp_ajax_nopriv_astro_ajax_loop_result', [$this, 'loop_result']);

        add_action('wp_enqueue_scripts', [$this, 'load_scripts']);

    }

    /**
     * return query
     *
     * @return [post]
     */
    public function loop_result()
    {

        if (check_ajax_referer('astro-element-loop-nonce', 'check_nonce')) {
            // get setting form json
            $settings = $_POST['settings'];



            // get post archive or block form elementor
            $block = $_POST['block'];

            $query_standard = json_decode(stripslashes($_POST['query']), true);

            // if null
            $query_by = array();

            $query_by = array(
                'post_type' => $settings['post_type'],
                'paged' => $_POST['page'],
                'query_by' => $settings['query_by'],
                'orderby' => $settings['orderby'],
                'order' => $settings['order'],
                'posts_per_page' => $settings['posts_per_page'],
            );

            // Merge Array
            $args = Helper::query($query_by);

            $the_query = new \WP_Query($args);
            if ($the_query->have_posts()) {
                while ($the_query->have_posts()): $the_query->the_post();

                   include ASTRO_ELEMENT_TEMPLATE . "/{$settings['template_part']}.php";

                endwhile;
                wp_reset_postdata();
            } else {
                _e('No Result', 'astro-element');
            }
        }

        wp_die();
    }

    /**
     * load scripts
     *
     * @return [inject ajax-loop.js, js variable]
     */
    public function load_scripts()
    {
        global $wp_query;

        $max = $wp_query->max_num_pages;
        $paged = get_query_var('paged') ? get_query_var('paged') : 1;

        wp_enqueue_script('astro_ajax_loop',  ASTRO_ELEMENT_ASSETS.'assets/js/ajax-loop.js', array('jquery'));

        wp_localize_script(
            'astro_ajax_loop',
            'astro_ajax_loop',
            array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'check_nonce' => wp_create_nonce('astro-element-loop-nonce'),
                'posts' => json_encode($wp_query->query_vars),
            )
        );
    }

    /* end class */
}

new Class_Ajax();

