
<?php do_settings_sections("retheme_section");?>

<div class="bulma theme-panel">
    <section class="page-header">
        <h1 class="title"><?php echo ASTRO_ELEMENT_NAME ?> Lisensi</h1>
    </section>

    <div class="panel bg-light">

        <div class="panel-heading">
            Lisensi Informasi

            <?php if (ael_is_local()): ?>
                <span class="tag is-success">Local Lisensi</span>
            <?php elseif (ael_is_premium()): ?>
                <span class="tag is-success">Activate</span>
            <?php else: ?>
                <span class="tag is-warning">Deactivate</span>
            <?php endif?>

        </div>



        <div class="panel-block">
            <div>

                <?php if (ael_is_local()): ?>
                  <div style="margin: 15px 0;">Anda sedang menggunakan plugin ini di localhost server, semua fitur <strong>Premium</strong> akan aktif pada server lokal. Anda wajib memasukan lisensi ketika di hosting atau server online </div>
                <?php else: ?>
                <div style="margin: 15px 0;">Masukan kunci lisensi Anda untuk dapat menggunakan semua fitur premium, update, dan support. Bila Anda tidak memiliki kunci lisensi silakan klik <a target="_blank" href="<?php echo ASTRO_ELEMENT_URL?>">disini</a></div>
                <?php endif?>

                <form action="" method="post">
                    <div class="field">
                        <label class="label">Lisensi Key</label>

                        <?php if (ael_is_local()): ?>
                            <input class="input" id="astro_element_license_key" name="astro_element_license_key" type="password" value="localhost" disabled />
                        <?php elseif (ael_is_premium()): ?>
                            <input class="input" type="password" value="<?php echo ASTRO_ELEMENT_SLUG.'_key'?>" disabled/>
                            <input name="astro_element_license_key" id="astro_element_license_key" type="hidden" value="<?php echo get_option(ASTRO_ELEMENT_SLUG.'_key'); ?>"/>
                        <?php else: ?>
                            <input class="input" id="astro_element_license_key" name="astro_element_license_key" type="text" value="<?php echo get_option(ASTRO_ELEMENT_SLUG.'_key'); ?>"/>
                        <?php endif?>

                    </div>
                    <div class="submit">

                        <?php if (ael_is_local()): ?>
                            <input class="button is-info" name="astro_element_license_submit" type="submit" value="Activate License" disabled />
                        <?php elseif (ael_is_premium()): ?>
                             <input class="button is-info" name="astro_element_license_submit" type="submit" value="Deactivate License"/>
                        <?php else: ?>
                            <input class="button is-info" name="astro_element_license_submit" type="submit" value="Activate License"/>
                        <?php endif?>

                    </div>
                </form>

            </div>

        </div>

    </div>

    <!-- gmap -->
    <div class="panel bg-light">
        <div class="panel-heading">Gmap API</div>
        <div class="panel-block">
            <div>
                <form action="" method="post">
                    <div class="field">
                            <label class="label">Gmap API</label>
                            <input class="input" name="astro_element_gmap_api" type="text" value="<?php echo get_option('astro_element_gmap_api'); ?>"/>
                        </div>
                        <div class="submit">
                            <input class="button is-info" type="submit" name="astro_element_gmap_submit" value="Submit">
                        </div>
                </form>
            </div>
        </div>
    </div>

</div>