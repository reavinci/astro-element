<?php
namespace Astro_Element\Admin;

class Admin
{
    public function __construct()
    {
        $this->include_part();
        add_action('admin_enqueue_scripts', array($this, 'register_scripts'));

    }

    public function include_part()
    {
        include_once dirname(__FILE__) . '/options.php';
        include_once dirname(__FILE__) . '/admin-function.php';

    }

    public function register_scripts()
    {
        wp_enqueue_style('retheme-base', ASTRO_ELEMENT_ASSETS . '/core/admin/assets/css/retheme-base.min.css');
        wp_enqueue_style('retheme-admin', ASTRO_ELEMENT_ASSETS . '/core/admin/assets/css/retheme-admin.css');
        wp_enqueue_script('retheme-admin', ASTRO_ELEMENT_ASSETS . '/core/admin/assets/js/retheme-admin.js', array('jquery'), '1.0.0', true);
    }
}

new Admin();
