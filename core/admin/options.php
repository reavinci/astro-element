<?php

namespace Astro_Element\Admin;

class Options
{

    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_menu_panel'));
        add_action("admin_init", array($this, 'store'));


    }

    /**
     * Show plugin menu
     *
     * @return void
     */
    public function add_menu_panel()
    {

        add_submenu_page('options-general.php', 'Astro Elements', 'Astro Elements', 'edit_posts', 'astro_elements', function(){
            include ASTRO_ELEMENT_TEMPLATE . 'core/admin/views/license.php';
        });

    }


    /*
    * Store gmap API key
    */
    public function store(){

        if(!empty($_POST['astro_element_gmap_submit']) && !empty($_POST['astro_element_gmap_api'])){
            $api = $_POST['astro_element_gmap_api'];
            update_option('astro_element_gmap_api', $api);
        }
        
    }

}

new Options;
